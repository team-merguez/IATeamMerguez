import client.Client;
import client.TcpGrandOrdonnateur;
import util.Log;

/**
 * Classe principale.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Timothé PARDIEU
 *         Robin LEPETIT
 */
public class Main {
    public static void main(String[] args) {
        if (args.length != 2 || !args[1].matches("[0-9]+")) {
            System.err.println("USAGE: java " + Main.class.getName() + " <hôte> <port>");
            System.exit(1);
        }

        try {
            TcpGrandOrdonnateur tcpGrandOrdonnateur = new TcpGrandOrdonnateur();

            tcpGrandOrdonnateur.connexion(args[0], Integer.parseInt(args[1]));
            Client client = new Client(tcpGrandOrdonnateur, "Team Merguez", "Le Havre");

            Log.info("Le client est en écoute");
            client.ecouter();
        } catch (Exception e) {
            Log.error("Connexion au serveur impossible !");
            e.printStackTrace();
        }
    }
}
