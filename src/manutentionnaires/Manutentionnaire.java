package manutentionnaires;

import cave.Salle;
import cave.SalleCasier;
import cave.SalleEscalier;
import util.Log;

/**
 * Classe de gestion d'un manutentionnaire de la cave.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Timothé PARDIEU
 *         Robin LEPETIT
 */
public class Manutentionnaire {

    /**
     * Nombre de manutentionnaires maximum que la cave peut accueillir.
     */
    public static final int NB_MANUTENTIONNAIRES_MAX = 3;

    /**
     * Sac du manutentionnaire.
     */
    public final Sac sac;

    /**
     * La salle où se trouve le manutentionnaire.
     */
    protected Salle salle;

    /**
     * Création du manutentionnaire.
     */
    public Manutentionnaire(Salle salleDepart) {
        this.sac = new Sac();
        this.salle = salleDepart;
    }

    /**
     * Fait ecouter le manutentionnaire avec les actions passées en paramètres.
     *
     * @param actions actions que doit effectuer le manutentionnaire
     */
    public void jouer(String actions) {
        for (Character c : actions.toCharArray()) {
            EAction action = EAction.getEAction(c);

            if (action != null)
                action.effectuer(this);
        }
    }

    /**
     * Enumération des actions possibles d'un manutentionnaire.
     */
    protected enum EAction {
        NORD('N') {
            @Override
            public boolean effectuer(Manutentionnaire m) {
                if (m.salle.getSalleNord() != null) {
                    m.salle = m.salle.getSalleNord();
                    return true;
                }
                return false;
            }
        },
        EST('E') {
            @Override
            public boolean effectuer(Manutentionnaire m) {
                if (m.salle.getSalleEst() != null) {
                    m.salle = m.salle.getSalleEst();
                    return true;
                }
                return false;
            }
        },
        SUD('S') {
            @Override
            public boolean effectuer(Manutentionnaire m) {
                if (m.salle.getSalleSud() != null) {
                    m.salle = m.salle.getSalleSud();
                    return true;
                }
                return false;
            }
        },
        OUEST('O') {
            @Override
            public boolean effectuer(Manutentionnaire m) {
                if (m.salle.getSalleOuest() != null) {
                    m.salle = m.salle.getSalleOuest();
                    return true;
                }
                return false;
            }
        },
        POSER('P') {
            @Override
            public boolean effectuer(Manutentionnaire m) {
                return m.salle instanceof SalleCasier && ((SalleCasier) m.salle).poser(m.sac);
            }
        },
        REMPLIR('R') {
            @Override
            public boolean effectuer(Manutentionnaire m) {
                return m.salle instanceof SalleEscalier && ((SalleEscalier) m.salle).remplirSac(m.sac);
            }
        },
        INTERROMPRE('I') {
            @Override
            public boolean effectuer(Manutentionnaire m) {
                return false;
            }
        },;

        /**
         * Le caractère d'effectuer reconnu par le serveur.
         */
        private char action;

        /**
         * Construction de l'effectuer.
         *
         * @param action le caractère d'effectuer reconnu par le serveur.
         */
        EAction(char action) {
            this.action = action;
        }

        /**
         * Retourne l'effectuer correspondant au type d'effectuer passé
         * en paramètre.
         *
         * @param action le type de l'effectuer
         * @return null si aucune effectuer ne correspond
         */
        public static EAction getEAction(char action) {
            for (EAction a : EAction.values())
                if (a.action == action)
                    return a;

            return null;
        }

        /**
         * @return le caractère correspondant à l'action
         */
        public char getAction() {
            return this.action;
        }

        /**
         * Effectue une effectuer précise selon le type d'effectuer
         * avec le manutentionnaore passé en paramètre.
         *
         * @param m le manutentionnaire effectuant l'effectuer
         * @return vrai si l'effectuer a été effectuée
         */
        public abstract boolean effectuer(Manutentionnaire m);
    }

    /**
     * Classe de gestion du sac du manutentionnaire.
     */
    public class Sac {

        /**
         * Nombre de bouteilles maximum que le sac peut contenir.
         */
        private static final int MAX_BOUTEILLES = 10;

        /**
         * Nombre de bouteilles contenues dans le sac.
         */
        private int nbBouteilles = 10;

        /**
         * @return le nombre de bouteilles contenues dans le sac
         */
        public int getNbBouteilles() {
            return nbBouteilles;
        }

        /**
         * Ajoute une bouteille dans le sac du manutentionnaire s'il n'est
         * pas déjà plein.
         * Une pénalité sera infilgée s'il le sac est déjà plein.
         *
         * @ vrai si on a pu ajouter la bouteille dans le sac
         */
        public boolean ajouter() {
            if (this.nbBouteilles < Sac.MAX_BOUTEILLES) {
                this.nbBouteilles++;
                return true;
            }

            return false;
        }

        /**
         * Enlève une bouteille du sac s'il en reste.
         *
         * @return vrai si la bouteille a pu être retirée
         */
        public boolean enlever() {
            if (this.nbBouteilles > 0) {
                this.nbBouteilles--;
                return true;
            }

            return false;
        }

        /**
         * @return vrai si le sac est plein
         */
        public boolean estPlein() {
            return this.nbBouteilles >= Sac.MAX_BOUTEILLES;
        }

        /**
         * @return vrai si le sac est vide
         */
        public boolean estVide() {
            return this.nbBouteilles <= 0;
        }
    }
}
