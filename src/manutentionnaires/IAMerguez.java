package manutentionnaires;

import cave.Salle;

/**
 * Classe d'un manutentionnaire autonome (ses actions sont calculées automatiquement).
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Timothé PARDIEU
 *         Robin LEPETIT
 */
public class IAMerguez extends Manutentionnaire {

    /**
     * Création du manutentionnaire.
     *
     * @param salleDepart
     */
    public IAMerguez(Salle salleDepart) {
        super(salleDepart);
    }


    public String jouer() {
        StringBuilder sActions = new StringBuilder();
        Salle meilleurSalle = this.getMeilleureSalle();

        for (int i = 0; i < 7 && meilleurSalle != null; i++) {
            if (EAction.POSER.effectuer(this)) {
                sActions.append(EAction.POSER.getAction());
            } else if (this.salle == meilleurSalle && EAction.REMPLIR.effectuer(this)) {
                sActions.append(EAction.REMPLIR.getAction());
            } else if (this.salle.getPosition().getY() > meilleurSalle.getPosition().getY()) {
                sActions.append(EAction.NORD.getAction());
                this.salle = this.salle.getSalleNord();
            } else if (this.salle.getPosition().getY() < meilleurSalle.getPosition().getY()) {
                sActions.append(EAction.SUD.getAction());
                this.salle = this.salle.getSalleSud();
            } else if (this.salle.getPosition().getX() > meilleurSalle.getPosition().getX()) {
                sActions.append(EAction.OUEST.getAction());
                this.salle = this.salle.getSalleOuest();
            } else if (this.salle.getPosition().getX() < meilleurSalle.getPosition().getX()) {
                sActions.append(EAction.EST.getAction());
                this.salle = this.salle.getSalleEst();
            } else {
                meilleurSalle = this.getMeilleureSalle();
                i--;
            }
        }

        // Sécurité
        if (sActions.length() < 7) sActions.append(EAction.INTERROMPRE.getAction());

        return sActions.toString();
    }

    /**
     * @return la meilleure salle où l'IA doit aller
     */
    private Salle getMeilleureSalle() {
        if (this.sac.estVide() || !this.sac.estPlein() && this.salle == this.salle.getCave().getEscalier())
            return this.salle.getCave().getEscalier();
        else
            return this.salle.getCave().getMeilleurSalle(this.salle);
    }

    /**
     * @return la distance entre l'escalier et la salle courante du manutentionnaire
     */
    private int getDistanceEscalier() {
        return this.salle.getDistanceDe(this.salle.getCave().getEscalier());
    }
}
