package client;
/**
 * Illustration du protocole du Grand Ordonnateur.
 */

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

/**
 * Illustration du protocole du Grand Ordonnateur, fournie par le gentil
 * organisateur de l'�preuve de d�veloppement des 24h IUT Informatique de
 * Bordeaux 2016 (OG&OG).
 *
 * @author Olivier
 */
public class IllustrProtoc {

    /**
     * H�te du Grand Ordonnateur.
     */
    private String hote = null;

    /**
     * Port du Grand Ordonnateur.
     */
    private int port = -1;

    /**
     * Interface pour le protocole du Grand Ordonnateur.
     */
    private TcpGrandOrdonnateur tcpGdOrdo = null;

    /**
     * Cr�ation d'une illustration du protocole du Grand Ordonnateur.
     *
     * @param hote H�te.
     * @param port Port.
     */
    public IllustrProtoc(String hote, int port) {
        this.hote = hote;
        this.port = port;
        this.tcpGdOrdo = new TcpGrandOrdonnateur();
    }

    /**
     * Programme principal.
     *
     * @param args Arguments.
     */
    public static void main(String[] args) {
        System.out.println("D�marrage de notre application Java ce "
                + (DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL)).format(new Date()) + ".");
        System.out.flush();
        // � create � du protocole du Grand Ordonnateur.
        final String USAGE = System.lineSeparator() + "\tUsage : java " + IllustrProtoc.class.getName()
                + " <h�te> <port>";
        if (args.length != 2) {
            System.out.println("Nombre de param�tres incorrect." + USAGE);
            System.out.flush();
            System.exit(1);
        }
        String hote = args[0];
        int port = -1;
        try {
            port = Integer.valueOf(args[1]);
        } catch (NumberFormatException e) {
            System.out.println("Le port doit �tre un entier." + USAGE);
            System.out.flush();
            System.exit(1);
        }
        try {
            (new IllustrProtoc(hote, port)).scenario();
        } catch (IOException e) {
            System.out.println("Erreur lors de la cr�ation d'une illustration du protocole du Grand Ordonnateur.");
            System.out.flush();
            e.printStackTrace();
            System.err.flush();
            System.exit(1);
        }
    }

    /**
     * Sc�nario illustrant le protocole du Grand Ordonnateur.
     * <p>
     * Cf. TcpGrandOrdonnateur.chaineValidePourTransmission pour conna�tre les
     * caract�res autoris�s par le protocole du Grand Ordonnateur.
     *
     * @throws IOException
     */
    private void scenario() throws IOException {
        // Finalisation du � create � du protocole du Grand Ordonnateur.
        // - connexion
        tcpGdOrdo.connexion(hote, port);
        // R�ponse au � create � du protocole du Grand Ordonnateur.
        // - envoi du nom de notre �quipe
        tcpGdOrdo.envoiChaine("nom de notre equipe");
        // - envoi de l'IUT o� les membres de notre �quipe �tudient
        tcpGdOrdo.envoiChaine("IUT ou nous etudions Java");
        // Initialisation du protocole du Grand Ordonnateur.
        // - r�ception du nombre de lignes de la cave
        tcpGdOrdo.receptionEntier();
        // - r�ception du nombre de colonnes de la cave
        tcpGdOrdo.receptionEntier();
        // - r�ception des casiers (nombre d'emplacements et position de
        // l'escalier) de la cave
        tcpGdOrdo.receptionChaine();
        // - r�ception du nombre de manutentionnaires ; 3 ici
        tcpGdOrdo.receptionEntier();
        // - r�ception de l'ordre (entre 1 et le nombre de manutentionnaires)
        // dans lequel l'application singeant notre manutentionnaire joue ; 2
        // ici
        tcpGdOrdo.receptionEntier();
        // Premier tour de jeu :
        // - r�ception des actions du premier manutentionnaire
        tcpGdOrdo.receptionChaine();
        // - envoi de nos actions (comme deuxi�me manutentionnaire)
        // diff�remment du second tour de jeu
        // N. B. : n'utilisez pas tcpGdOrdo.envoiChaine()
        tcpGdOrdo.envoiCaractere('E');
        tcpGdOrdo.envoiCaractere('P');
        tcpGdOrdo.envoiCaractere('P');
        tcpGdOrdo.envoiCaractere('O');
        tcpGdOrdo.envoiCaractere('O');
        tcpGdOrdo.envoiCaractere('P');
        tcpGdOrdo.envoiCaractere('P');
        // - r�ception des actions du troisi�me manutentionnaire
        tcpGdOrdo.receptionChaine();
        // Second tour de jeu :
        // - r�ception des actions du premier manutentionnaire
        tcpGdOrdo.receptionChaine();
        // - envoi de nos actions (comme deuxi�me manutentionnaire)
        // diff�remment du premier tour de jeu
        // N. B. : n'utilisez pas tcpGdOrdo.envoiChaine()
        for (char action : "EREPEI".toCharArray()) {
            tcpGdOrdo.envoiCaractere(action);
        }
        // - r�ception des actions du troisi�me manutentionnaire
        tcpGdOrdo.receptionChaine();
        // Remarque : le reste du code de cette m�thode est purement esth�tique
        // puisque l'application va �tre arr�t�e par un � destroy � du protocole
        // du Grand Ordonnateur.
        // - attente
        try {
            Thread.sleep(1 * 60 * 1000);
        } catch (InterruptedException e) {
            System.out.println("Erreur lors de l'attente du � destroy � du protocole du Grand Ordonnateur.");
            System.out.flush();
            e.printStackTrace();
            System.err.flush();
            System.exit(1);
        }
        // - d�connexion
        try {
            tcpGdOrdo.deconnexion();
        } catch (IOException e) {
            // Qu'importe si le serveur s'est arr�t� avant le client.
        }
    }

}
