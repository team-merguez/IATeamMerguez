package client;

import cave.Cave;
import manutentionnaires.IAMerguez;
import manutentionnaires.Manutentionnaire;
import util.Log;

import java.io.IOException;

/**
 * Classe de gestion de l'envoie et de la réception des
 * ordres entre le sereveur et ce client.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Timothé PARDIEU
 *         Robin LEPETIT
 */
public class Client {

    /**
     * Représentation identique de la cave du serveur.
     */
    private Cave cave;

    /**
     * Gestion de la connexion TCP avec le serveur.
     */
    private TcpGrandOrdonnateur tcpGrandOrdonnateur;

    /**
     * Construction du client en se connectant à l'hote @code{hote} sur le port @code{port}
     * avec comme nom d'équipe @code{nomEquipe} et comme IUT @code{iut}
     *
     * @param nomEquipe le nom de l'equipe qui joue
     * @param iut       l'iut auquel appartient l'equipe
     */
    public Client(TcpGrandOrdonnateur tcpGrandOrdonnateur, String nomEquipe, String iut) throws IOException {

        this.tcpGrandOrdonnateur = tcpGrandOrdonnateur;

        /*--------------------------*/
        /* Identification de client */
        /*--------------------------*/

        this.tcpGrandOrdonnateur.envoiChaine(nomEquipe); // envoi du nom de notre équipe
        this.tcpGrandOrdonnateur.envoiChaine(iut);     // envoi de l'IUT où les membres de notre équipe étudient

        /*---------------------------------*/
        /* Récupération des données de jeu */
        /*---------------------------------*/

        int nbLigCave = this.tcpGrandOrdonnateur.receptionEntier(); // réception du nombre de lignes de la cave
        int nbColCave = this.tcpGrandOrdonnateur.receptionEntier(); // réception du nombre de colonnes de la cave
        String casiers = this.tcpGrandOrdonnateur.receptionChaine(); // réception des casiers
        int nbManu = this.tcpGrandOrdonnateur.receptionEntier(); // réception du nombre de manutentionnaires
        int numManu = this.tcpGrandOrdonnateur.receptionEntier(); // réception de l'ordre (entre 1 et le nombre de manutentionnaires) dans lequel l'application singeant notre manutentionnaire joue;

        /*------------------------*/
        /* Initialisation du jeu */
        /*-----------------------*/

        this.cave = new Cave(nbLigCave, nbColCave, casiers, nbManu);

        while (nbManu > 0) {
            if (nbManu == numManu)
                this.cave.ajouterManutentionnaire(new IAMerguez(this.cave.getEscalier()));
            else
                this.cave.ajouterManutentionnaire(new Manutentionnaire(this.cave.getEscalier()));

            nbManu--;
        }
    }

    /**
     * Le client écoute sur le tcpGrandOrdonnateur jusqu'à
     * ce que celui-ci ferme la connexion.
     */
    public void ecouter() {
        boolean estConnecte = true;

        while (estConnecte) {
            for (int i = 0; i < this.cave.getNbManutentionnaires(); i++) {
                Manutentionnaire m = this.cave.getManutentionnaire(i);

                try {
                    if (m instanceof IAMerguez) {
                        Log.info("IAMerguez joue !");
                        String sActions = ((IAMerguez) m).jouer();
                        for (Character c : sActions.toCharArray())
                            this.tcpGrandOrdonnateur.envoiCaractere(c);
                    } else {
                        Log.info("L'IA adverse joue");
                        String sActions = this.tcpGrandOrdonnateur.receptionChaine();
                        if (sActions != null)
                            m.jouer(sActions);
                    }
                } catch (IOException e) {
                    estConnecte = false;
                    break;
                }
            }
        }
    }
}
