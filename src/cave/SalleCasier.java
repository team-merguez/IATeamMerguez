package cave;

import manutentionnaires.Manutentionnaire;
import util.Position;

/**
 * Classe de gestion d'une salle de la cave contenant un casier.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Timothé PARDIEU
 *         Robin LEPETIT
 */
public class SalleCasier extends Salle {

    /**
     * Nombre de casier de la salle.
     */
    private final int nbCasiers;

    /**
     * Nombre de bouteilles contenues dans les casiers.
     */
    private int nbBouteilles;

    /**
     * Construction d'une salle contenant un casier.
     *
     * @param cave      la cave contenant la salle
     * @param position  position de la salle dans la cave
     * @param nbCasiers nombre de casiers de la salle
     */
    SalleCasier(Cave cave, Position position, int nbCasiers) {
        super(cave, position);

        this.nbCasiers = nbCasiers;
        this.nbBouteilles = 0;
    }

    /**
     * Pose une bouteille si le casier n'est pas plein.
     */
    public boolean poser(Manutentionnaire.Sac sac) {
        if (this.nbBouteilles < this.nbCasiers && sac.enlever()) {
            this.nbBouteilles++;
            return true;
        }

        return false;
    }

    /**
     * @return le nombre d'emplacement disponibles dans le casier
     */
    public int getNbPlacesDisponibles() {
        return this.nbCasiers - this.nbBouteilles;
    }

    /**
     * Calcule le nombre de points que peut rapporter la salle.
     */
    public int calculerPoints() {
        return this.getNbPlacesDisponibles() * 4 * (this.cave.getEscalier().getDistanceDe(this) + 1);
    }
}
