package cave;

import manutentionnaires.Manutentionnaire;
import util.Position;

/**
 * Classe de gestion d'une salle de la cave contenant un escalier.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Timothé PARDIEU
 *         Robin LEPETIT
 */
public class SalleEscalier extends Salle {

    /**
     * Symbole représentant une salle contenant un escalier.
     */
    static final char SYMBOLE = '@';

    /**
     * Construction de la salle.
     *
     * @param cave la cave contenant la salle
     */
    SalleEscalier(Cave cave, Position position) {
        super(cave, position);
    }

    /**
     * Remplie le sac d'un manutentionnaire.
     */
    public boolean remplirSac(Manutentionnaire.Sac sac) {
        return !sac.estPlein() && sac.ajouter();
    }
}
