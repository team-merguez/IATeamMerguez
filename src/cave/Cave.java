package cave;

import manutentionnaires.Manutentionnaire;
import util.Position;

/**
 * Classe de gestion de la cave.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Timothé PARDIEU
 *         Robin LEPETIT
 */
public class Cave {

    /**
     * Nombre des lignes de la cave.
     */
    private int nbLignes;

    /**
     * Nombre de colonnes de la cave.
     */
    private int nbColonnes;

    /**
     * Manutentionnaires présents dans la cave.
     */
    private Manutentionnaire[] manutentionnaires;

    /**
     * Nombre de manutentionnaires présents dans la cave.
     */
    private int nbManutentionnaires;

    /**
     * Salles de la cave.
     */
    private Salle[][] salles;

    /**
     * Salle où se trouve l'escalier.
     */
    private SalleEscalier escalier;

    /**
     * Construction de la cave.
     */
    public Cave(int nbLignes, int nbColonnes, String casiers, int nbManutentionnaires) {
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;

        this.manutentionnaires = new Manutentionnaire[nbManutentionnaires];
        this.salles = new Salle[nbLignes][nbColonnes];

        this.initSalles(casiers);
    }

    /**
     * Initialisation de chaque de salle de la cave en détermiant le bon type de salle.
     *
     * @param casiers chaîne de caractères contenant la disposition de la cave
     */
    private void initSalles(String casiers) {
        // Création des salles
        for (int i = 0; i < this.nbLignes; i++) {
            for (int j = 0; j < this.nbColonnes; j++) {
                char c = casiers.charAt(i * this.nbColonnes + j);

                // Détermination du type de la salle
                if (c == SalleEscalier.SYMBOLE && this.escalier == null) {
                    this.salles[i][j] = this.escalier = new SalleEscalier(this, new Position(j, i));
                } else if (c >= '0' && c <= '9') {
                    this.salles[i][j] = new SalleCasier(this, new Position(j, i), Integer.parseInt(Character.toString(c)));
                }
            }
        }
    }

    /**
     * Retourne la salle à la ligne et à la colonne passées en paramètre
     *
     * @param ligne   la ligne de la salle
     * @param colonne la colonne de la salle
     * @return la salle
     */
    public Salle getSalle(int ligne, int colonne) {
        if (ligne < 0 || ligne >= this.salles.length || colonne < 0 || colonne >= this.salles[ligne].length)
            return null;

        return this.salles[ligne][colonne];
    }

    /**
     * @return la salle contenant l'escalier
     */
    public SalleEscalier getEscalier() {
        return escalier;
    }

    /**
     * @return le nombre de manutentionnaires présents dans la salle
     */
    public int getNbManutentionnaires() {
        return this.nbManutentionnaires;
    }

    /**
     * Retourne le manutentionnaire d'indice @code{indice}
     *
     * @param indice l'indice du manutentionnaire
     * @return le manutentionnaire s'il existe sinon null
     */
    public Manutentionnaire getManutentionnaire(int indice) {
        if (indice >= 0 && indice < this.nbManutentionnaires)
            return this.manutentionnaires[indice];

        return null;
    }

    /**
     * Ajoute un manutentionnaire dans la cave.
     *
     * @param m le manutentionnaire à ajouter
     * @return vrai si le manutentionnaire a pu être ajouté dans la cave
     */
    public boolean ajouterManutentionnaire(Manutentionnaire m) {
        if (manutentionnaires != null && this.nbManutentionnaires < this.manutentionnaires.length) {
            this.manutentionnaires[this.nbManutentionnaires++] = m;
            return true;
        }

        return false;
    }

    /**
     * Retourne la salle qui donne le meilleur rendement.
     *
     * @param salleCourante la salle où se trouve le manutentionnaire
     * @return la salle
     */
    public Salle getMeilleurSalle(Salle salleCourante) {
        Salle s = null;
        int apport = 0;

        for (int i = 0; i < this.salles.length; i++) {
            for (int j = 0; j < this.salles[i].length; j++) {
                if (this.salles[i][j] instanceof SalleCasier) {
                    // Apport possible en posant des bouteilles - la distance à parcourir
                    int apportTmp = ((SalleCasier) this.salles[i][j]).calculerPoints() - salleCourante.getDistanceDe(this.salles[i][j]);

                    if (apportTmp > apport) {
                        s = this.salles[i][j];
                        apport = apportTmp;
                    }
                }
            }
        }

        return s;
    }
}
