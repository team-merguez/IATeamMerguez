package cave;

import util.Position;

/**
 * Classe de gestion d'une salle de la cave.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Timothé PARDIEU
 *         Robin LEPETIT
 */
public abstract class Salle {

    /**
     * La cave contenant la salle.
     */
    protected Cave cave;

    /**
     * Position de la salle dans la cave.
     */
    protected Position position;

    /**
     * Construction de la salle.
     *
     * @param cave la cave contenant la salle
     */
    Salle(Cave cave, Position position) {
        this.cave = cave;
        this.position = position;
    }

    /**
     * @return la salle qui se situe au nord de cette salle
     */
    public Salle getSalleNord() {
        return this.cave.getSalle(this.position.getY() - 1, this.position.getX());
    }

    /**
     * @return la salle qui se situe à l'est de cette salle
     */
    public Salle getSalleEst() {
        return this.cave.getSalle(this.position.getY(), this.position.getX() + 1);
    }

    /**
     * @return la salle qui se situe au sud de cette salle
     */
    public Salle getSalleSud() {
        return this.cave.getSalle(this.position.getY() + 1, this.position.getX());
    }

    /**
     * @return la salle qui se situe à l'ouest de cette salle
     */
    public Salle getSalleOuest() {
        return this.cave.getSalle(this.position.getY(), this.position.getX() - 1);
    }

    /**
     * @return la cave contenant la salle.
     */
    public Cave getCave() {
        return cave;
    }

    /**
     * @return la position de la salle
     */
    public Position getPosition() {
        return position;
    }

    /**
     * Calcule et retourne la distance entre cet salle et une autre.
     *
     * @param autre l'autre salle
     * @return la distance entre ces deux salles
     */
    public int getDistanceDe(Salle autre) {
        return Math.abs(this.position.getY() - autre.position.getY()) +
                Math.abs(this.position.getX() - autre.position.getX());
    }
}
