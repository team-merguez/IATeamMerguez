package util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe de visualisation des logs.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Timothé PARDIEU
 *         Robin LEPETIT
 */
public class Log {

    /**
     * Default shell's color.
     */
    private static final String SHELL_COLOR_BLACK = "\033[0m";

    /**
     * Affichage ou non des messages de débug.
     */
    private static final boolean DEBUG_MODE = true;

    /**
     * Affiche un message d'information dans la console.
     *
     * @param s    le message a afficher
     * @param args les arguments du message
     */
    public static void info(Object s, Object... args) {
        print("[INFO]" + String.valueOf(s), "", args);
    }

    /**
     * Affiche un message d'alerte dans la console.
     *
     * @param s    le message a afficher
     * @param args les arguments du message
     */
    public static void warning(Object s, Object... args) {
        print("[WARNING]" + String.valueOf(s), "\033[1;33m", args);
    }

    /**
     * Affiche un message d'erreur dans la console.
     *
     * @param s    le message a afficher
     * @param args les arguments du message
     */
    public static void error(Object s, Object... args) {
        print("[ERROR]" + String.valueOf(s), "\033[1;31m", args);
    }

    /**
     * Affiche un message de debug dans la console.
     *
     * @param s    le message a afficher
     * @param args les arguments du message
     */
    public static void debug(Object s, Object... args) {
        if (Log.DEBUG_MODE) print("[DEBUG]" + String.valueOf(s), "\033[1;34m", args);
    }

    /**
     * Affiche un message dans la console.
     *
     * @param s     le message à afficher
     * @param color la coloration du message
     * @param args  les arguments du message
     */
    private static void print(Object s, String color, Object... args) {
        SimpleDateFormat formatter = new SimpleDateFormat("'['dd/MM/yyyy HH:mm:ss']'");

        System.out.println(color + formatter.format(new Date()) + String.format(String.valueOf(s), args) + SHELL_COLOR_BLACK);
    }
}
