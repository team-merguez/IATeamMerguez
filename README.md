IATeamMerguez
----
L'IAMerguez est une intelligence artificielle conçue spécialement pour répondre au sujet de l'épreuve "Développement d'application" des 24h des IUT Informatique 2016 qui ont eu lieu à Bordeaux.

L'épreuve consistait en le développement d'une IA qui pose des bouteilles dans une cave à vin et lui rapporte ainsi des points en fonciton de la distance salle/escalier.

Le développement se faisait en deux étapes :
- Connexion au serveur avec gestion de la réception et l'envoie des données.
- Développement d'une IA qui prend les meilleures décisions pour marquer un maximum de points.